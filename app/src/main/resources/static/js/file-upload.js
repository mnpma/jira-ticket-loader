//DO NOT USE !!!
const STATUS_INITIAL = 0, STATUS_SAVING = 1, STATUS_SUCCESS = 2, STATUS_FAILED = 3;

var uploadzip = Vue.component('uploadzip', {
    template: '#upload-template',
    data() {
        return {
            fileCount: 0,
            uploadedFiles: [],
            currentStatus: null,
            uploadFieldName: 'GentTemplateInstaller',
        }
    },
    computed: {
        isInitial() {
            return this.currentStatus === STATUS_INITIAL;
        },
        isSaving() {
            return this.currentStatus === STATUS_SAVING;
            console.log(this.currentStatus);
        },
        isSuccess() {
            return this.currentStatus === STATUS_SUCCESS;
        },
        isFailed() {
            return this.currentStatus === STATUS_FAILED;
        },
        uploadedFilesExist() {
            return this.uploadedFiles.length > 0;
        },
    },
    mounted() {
        this.reset();
    },
    methods: {
        reset() {
            // reset form to initial state
            this.currentStatus = STATUS_INITIAL;
            this.uploadedFiles = [];
        },
        save(formData) {
            // upload data to the server
            const url = '/srv/upload';
            var that = this;
            const config = {
                headers: {
                    'content-type': 'multipart/form-data'
                }
            }
            axios.post(url, formData, config)
                .then(function (response) {
                    console.log('STATUS_SUCCESS: ' + response)
                    //this.uploadedFiles = [].concat(response);
                    this.currentStatus = STATUS_SUCCESS;
                    var element = document.getElementById("dvLoading");
                    console.log('var element = document.getElementById("dvLoading"):' + element);
                    element.classList.remove("showloading");
                })
                .catch(function(error) {
                    var element = document.getElementById("dvLoading");
                    element.classList.remove("showloading");
                    console.log('ErrorMessage: '+error.response.data.message + ' Status: '+error.response.data.status)
                    this.currentStatus = STATUS_FAILED;
                    //remove the last added file to file's list
                    that.uploadedFiles.pop()
                    that.$emit('show-error', error.response.data.message + ' Status: '+error.response.data.status)
                });
        },
        filesChange(fieldName, fileList) {
            // handle file changes
            console.log('handle file changes')
            const formData = new FormData();
            if (!fileList.length) return;
            // append the files to FormData

            Array
                .from(Array(fileList.length).keys())
                .map(function(x) {
                    console.log(fileList[x].name);
                    this.uploadedFiles = [].concat(fileList[x].name);
                    console.log('fieldName: ' + fieldName)
                    formData.append('file', fileList[x], fileList[x].name);
                });
            var element = document.getElementById("dvLoading");
            element.classList.add("showloading");
            console.log('var element = document.getElementById("dvLoading"):'+element);
            window.setTimeout(null,3000);

            // save it
            this.save(formData);
        },
    }
});

