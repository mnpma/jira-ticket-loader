const STATUS_INITIAL = 0, STATUS_SAVING = 1, STATUS_SUCCESS = 2, STATUS_FAILED = 3;

var uploadzip = Vue.component('uploadzip', {
    template: '#upload-template',
    dvLoading: '#dvLoading',
    data() {
        return {
            fileCount: 0,
            uploadedFiles: [],
            currentStatus: null,
            uploadFieldName: 'file'
        }
    },
    computed: {
        isInitial() {
            return this.currentStatus === STATUS_INITIAL;
        },
        isSaving() {
            return this.currentStatus === STATUS_SAVING;
        },
        isSuccess() {
            return this.currentStatus === STATUS_SUCCESS;
        },
        isFailed() {
            return this.currentStatus === STATUS_FAILED;
        },
        uploadedFilesExist() {
            return this.uploadedFiles.length > 0;
        },
        isUploadFinish() {
            return this.currentStatus !== STATUS_SAVING;
        }
    },
    mounted() {
        this.reset();
    },
    methods: {

        reset() {
            // reset form to initial state
            this.currentStatus = STATUS_INITIAL;
        },
        save(formData) {
            // upload data to the server
            const url = '/srv/upload';
            var that = this;
            const config = {
                headers: {
                    'content-type': 'multipart/form-data'
                }
            }
            axios.post(url, formData, config)
                .then(function (response) {
                    console.log('STATUS_SUCCESS: ' + response)
                    that.currentStatus = STATUS_SUCCESS;
                    //remove spinner
                    var element = document.getElementById("ajaxBusy");
                    element.classList.remove("showloading");
                    //$('#ajaxBusy').hide();
                })
                .catch(function(error) {
                    var element = document.getElementById("ajaxBusy");
                    element.classList.remove("showloading");
                    //$('#ajaxBusy').hide();
                    console.log('ErrorMessage: '+error.response.data.message + ' Status: '+error.response.data.status)
                    that.currentStatus = STATUS_FAILED;
                    //remove the last added file to file's list
                    that.uploadedFiles.pop()
                    that.$emit('show-error', error.response.data.message + ' Status: '+error.response.data.status)
                });
        },
        filesChange(fieldName, fileList) {
            // handle file changes
            console.log('handle file changes')
            const formData = new FormData();
            if (!fileList.length) return;
            // append the files to FormData
            var that = this;
            Array
                .from(Array(fileList.length).keys())
                .map(function(x) {
                    console.log(fileList[x].name);
                    that.uploadedFiles.push(fileList[x].name) //= [].concat(fileList[x].name);
                    console.log('fieldName: ' + fieldName)
                    formData.append(fieldName, fileList[x], fileList[x].name);
                });
            this.currentStatus = STATUS_SAVING;
            //spinner
            var element = document.getElementById("ajaxBusy");
            element.classList.add("showloading");
            this.save(formData);
        },
    }
});

// register modal component
modal = Vue.component('modal', {
    template: '#modal-template',
    props: {
        itemkey: Number,
        itemName : String,
        itemType: String,
    },
    data() {
        return {
        }
    },

    methods: {
        confirmed(event) {
            //console.log("modal - confirmed is selected key from app: " + this.itemkey)
            if (event) event.preventDefault()
            this.$emit('close')
            if(this.itemKey !== undefined) {
                this.$emit('remove-item', this.itemkey, event)
            } else {
                this.$emit('remove-item', this.itemName, this.itemType, event)
            }
        }
    }
})
;

//register Table component
vueTable = Vue.component('vueTable', {
    props: {
        values : null,
        headers : null,
        warning: null
    },
    template: '#table-template',
    data: function () {
        return {
            ascending: false,
            sortColumn: '',
            showModal: false,
        };
    },
    computed: {
        rows() {
            return this.values
        },
        columns() {
            return Object.keys(this.values[0]);
        },
        headerList() {
            return this.headers;
        },
        colToWarning() {
            return this.warning;
        }
    },
    mounted() {
    },
    methods: {
        activateModal2(name, type11) {
            console.log('activate modal : name: ' + name);
            this.itemName = name;
            this.itemType = type11;
            this.showModal = true;
        },
        removeItemByName(itemName) {
            console.log('try to remove item : ' + itemName)
            this.$emit('remove-item', this.itemName, this.itemType, event)
        },
        sortTable: function sortTable(col) {
            if (this.sortColumn === col) {
                this.ascending = !this.ascending;
            } else {
                this.ascending = true;
                this.sortColumn = col;
            }

            var ascending = this.ascending;

            //this.rows.sort(
            this.values.sort(function(a, b) {
                if (a[col] > b[col]) {
                    return ascending ? 1 : -1
                } else if (a[col] < b[col]) {
                    return ascending ? -1 : 1
                }
                return 0;
            })
        },
    }
});

card=Vue.component('card', {
    template: '#app2',
    props: {
        test1: null,
        test2: null
    },
    data () { // opt. 1
        return {
            test1AsData: {...this.test1}
        }
    },
    computed: {// opt. 2
        test2AsComputed () {
            return {...this.test2}
        }
    }
});

/*
this example requires a modern browser that supports ES2015 natively
const modal = {
    template: '#modal-template',
}
*/
/*
components: {
    'modal': modal //ReferenceError: modal is not defined
},
*/
(function () {
    var app = new Vue({
        el: '#app',
        components: {'vue-table': vueTable},
        data() {
            return {
                tickets: [],
                errors: [],
                itemKey: 'Empty',
                showModal: false,
                isActive: false,
            }
        },
        computed: {
            quantity: function () {
                return this.tickets.length;
            },
            classObject: function () {
                return {
                    inactive: !this.errors || this.errors.length == 0,
                    active: this.errors && this.errors.length > 0,
                }
            },
        },
        mounted() {
/*
            setTimeout(() => {
                this.test1 = {1: 'updated!'}
                this.test2 = {2: 'updated!'}
            }, 1000);
*/
            this.fetchPosts();
        },
        methods: {
            fetchPosts() {
                axios.get("/rest/tickets").then(function (response) {
                    this.tickets = response.data;
                }.bind(this));
            },
            removeTicket(key) {
                console.log('removeTicket index: ' + key)
                this.tickets = tickets.filter(function(item){
                    /*if(item.id.toLowerCase().indexOf(key) !== -1)*/
                    if(item.id != key){
                        return item;
                    }
                })
                //this.tickets.splice(index, 1);
            },
            activateModal(key) {
                console.log('activate modal : key: ' + key);
                this.itemKey = key;
                this.showModal = true;
            },
            showError(errors, event) {
                //if (event) event.preventDefault()
                console.log('showError errors[]: ' + errors)
                this.isActive = errors
                console.log('isActive : ' + this.isActive)
                this.errors.push(errors)
            },
            removeItem(itemId) {
                console.log('try to remove item id: ' + itemId)
                var that = this;
                axios.delete("/rest/jiraticket/" + itemId)
                    .then(function (response) {
                        //alert(response)
                        that.removeTicket(itemId)
                        that.showError(null)
                    })
                    .catch(function(error) {
                        //"response": { "data": { "message": "exception.unexpected", "messages": null }, "status": 500, "statusText": ""
                        console.log(error.response.data.message + error.response.data.status)
                        that.showError([error.message, error.response.data.message + error.response.data.status])
                    })
            },
            removeItemBySummary(itemSummary, itemType) {
                var item = {'itemSummary':itemSummary, 'itemType':itemType}
                console.log('try to remove item :' + item.itemSummary + " " + item.itemType);
            }
        },
    });
})();