package com.hw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.hw", "com.hw.services"})
public class JiraTicketLoaderApplication {
	//Spring Boot will create a MultipartConfigElement bean and make itself ready for file uploads
	public static void main(String[] args) {
		SpringApplication.run(JiraTicketLoaderApplication.class, args);
	}
}
