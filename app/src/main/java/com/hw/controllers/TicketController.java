package com.hw.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.hw.TicketService;
import com.hw.dto.JiraResponse;
import com.hw.dto.Project;
import com.hw.dto.Ticket;

import com.hw.dto.TicketDto;
import com.hw.util.LocaleUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.hw.util.LocaleUtils.isValid;

@RestController
@RequestMapping(path = "/rest")
@Api("Ticket Api")
public class TicketController {
    private static final Logger log = LoggerFactory.getLogger(TicketController.class);
    @Autowired
    private TicketService ticketService;
    @Autowired
    private LocaleResolver localeResolver;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private HttpServletResponse response;

    private void prepareLocale(String lang) {
        Locale locale = LocaleUtils.parseLocale(lang);
        locale = isValid(locale) ? locale : Locale.US;
        localeResolver.setLocale(request, response, locale);
    }

    @ApiOperation(value = "Load CSV-file and return list of Ticket objects ",
            response = List.class)
    @RequestMapping(path = "/tickets", method = RequestMethod.GET)
    public List<TicketDto> getAllTickets(@RequestParam(value = "lang", defaultValue = "en", required = false) String lang) {
        prepareLocale(lang);
        log.debug("getAllTickets - load tickets from CSV-file");
        List<Ticket>  ticketList = ticketService.load();
        List<TicketDto> tickets = ticketService.convertTicketToTicketDto(ticketList);
        return tickets;//ResponseEntity.ok(tickets);
    }


    @ApiOperation(value = "Create Jira Ticket and return JiraResponse object.",
            response = List.class)
    @RequestMapping(path = "/jiraticket", method = RequestMethod.POST)
    public ResponseEntity<JiraResponse> createTicket(@RequestBody Ticket ticket,
                                                     @RequestParam(value = "lang", defaultValue = "en", required = false) String lang) throws JsonProcessingException {
        prepareLocale(lang);
        JiraResponse response = ticketService.create(ticket);
        return ResponseEntity.ok(response);
    }

    @RequestMapping(path = "/jiraticket/{issueKey}", method = RequestMethod.DELETE)
    public ResponseEntity deleteTicket(@PathVariable(name = "issueKey") String issueKey,
                                       @RequestParam(value = "lang", defaultValue = "en", required = false) String lang) {
        prepareLocale(lang);
        int response = ticketService.delete(issueKey);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "Health check and return Project object",
            response = Project.class)
    @RequestMapping(path = "/health")
    public ResponseEntity<Project> healthCheck(
            @RequestParam(value = "lang", defaultValue = "en", required = false) String lang) {
        prepareLocale(lang);
        Project project = ticketService.healthCheck();
        return ResponseEntity.ok(project);
    }


}
