#### Jira Ticket/s Loader

The application reads csv-file, view it's content, create jira ticket/s.

#### General principles of work

CSV file should be named the following way 
```
'jira-tickets.csv'
```

and should be found by path resources

#### JIRA Server platform REST API reference 
```
https://docs.atlassian.com/software/jira/docs/api/REST/7.6.1/#api/2/issue-createIssue
```

#### Build and Run tests:
```
cd jira-ticket-loader
mvnw install
```

#### Run
```
cd jira-ticket-loader
mvnw spring-boot:run -pl app
```

###### Documentation for REST API can be seen using http://localhost:9090/swagger-ui.html when app starts...

##### UI http://localhost:9090/

#### API methods :

##### GET /tickets 
#### Load CSV-file 'jira-tickets.csv'
##### URL:
```
http://localhost:9090/rest/tickets
```
##### Response:
```
{"id":0,
"issueType":null,
"project":null,
"status":null,
"priority":"Should Have",
"region":null,
"components":"WORP",
"summary":"WORP: create a new template for 50 State Surveys",
"subtasks":[
{"summary":"WORP: create a new template for 50 State Surveys-Dev",
"status":null,"priority":"3","issuetype":"5",
"timeTracking":{"originalEstimate":"5","remainingEstimate":"5","originalEstimateSeconds":432000,"remainingEstimateSeconds":432000}},
{"summary":"WORP: create a new template for 50 State Surveys-QED",
"status":null,"priority":"3","issuetype":"5",
"timeTracking":{"originalEstimate":"3","remainingEstimate":"3","originalEstimateSeconds":259200,"remainingEstimateSeconds":259200}},
{"summary":"WORP: create a new template for 50 State Surveys-CodeReview","status":null,"priority":"3","issuetype":"5",
"timeTracking":{"originalEstimate":"1","remainingEstimate":"1","originalEstimateSeconds":86400,"remainingEstimateSeconds":86400}},
{"summary":"WORP: create a new template for 50 State Surveys-UnitTests","status":null,"priority":"3","issuetype":"5",
"timeTracking":{"originalEstimate":"2","remainingEstimate":"2","originalEstimateSeconds":172800,"remainingEstimateSeconds":172800}}
],
"timetracking":null,
"reporter":null,
"assignee":null},
```

##### GET /health
##### Health Check with Basic Authorization
```
curl -D- -X GET -H "Authorization: Basic MjQxNzcxOjI0MTc3MQ==" -H "Content-Type: application/json" "http://jira.ald.int.westgroup.com/rest/api/latest/issue/ACT-34861"
```
##### Response:
```
HTTP/1.1 200 OK
Date: Fri, 08 Jun 2018 15:19:39 GMT
X-AREQUESTID: 619x1429871x1
X-XSS-Protection: 1; mode=block
X-Content-Type-Options: nosniff
X-Frame-Options: SAMEORIGIN
Content-Security-Policy: frame-ancestors 'self'
X-ASEN: SEN-2581805
X-Seraph-LoginReason: OK
X-ASESSIONID: 12lyyl2
X-AUSERNAME: 241771
Cache-Control: no-cache, no-store, no-transform
Content-Type: application/json;charset=UTF-8
Set-Cookie: JSESSIONID=D7BD4ABCE592205174BF0F1440B91FD8;path=/;HttpOnly
Set-Cookie: atlassian.xsrf.token=B6G4-KWNZ-148Y-9UJH|d15325e6fc806df9c1ae70584d9ad9ea4441f208|lin;path=/
Transfer-Encoding: chunked

{"expand":"renderedFields,names,schema,operations,editmeta,changelog,versionedRepresentations",
"id":"79413",
"self":"http://jira.ald.int.westgroup.com/rest/api/latest/issue/79413",
"key":"ACT-34861",
"fields":
    ...
    
```

##### POST /
#### Create one jira ticket for selected row in CSV-file
#### Task was created http://jira.ald.int.westgroup.com/rest/api/latest/issue/ACT-34995
##### Request Parameters:
```
http://localhost:9090/rest/jiraticket

{
	"summary": "create a new product (SQL scripts)",
	"timetracking": {
    	"originalEstimate": "2"
    }
}
```
The following JSON is sent to JIRA:
```
{"fields":
{"components":[{"id":"10213"}],
"project":{"id":"10106"},
"issuetype":{"id":"3"},

"priority":{"id":"4"},
"customfield_10703":{"id":"10702"},
"summary":"create a new product (SQL scripts)",
"assignee":{"name":"232642","emailAddress":"evgenii.podolskii@thomsonreuters.com"},
"reporter":{"name":"241771","emailAddress":"marina.pimenova@thomsonreuters.com"}}
}
```
##### Response:
```
Status 201 - application/jsonReturns a link to the created issue.
Example:
{
    "id": "10000",
    "key": "TST-24",
    "self": "http://www.example.com/jira/rest/api/2/issue/10000"
}
Status 400 Returned if the input is invalid (e.g. missing required fields, invalid field values, and so forth).
Example:
{
    "errorMessages": [
        "Field 'priority' is required"
    ],
    "errors": {}
} 
{
    "errorMessages": [],
    "errors": {
        "status": "Field 'status' cannot be set. It is not on the appropriate screen, or unknown."
    }
}
```

##### POST /create
#### Create jira tickets for all rows in CSV-file
##### Request Parameters:
```
```
##### Response:
```

```

#### JSON conversion

Spring uses the Jackson JSON library to automatically marshal instances of type <Ticket> into JSON
Jackson 2 is on the classpath, Spring’s MappingJackson2HttpMessageConverter is automatically chosen to convert the instance to JSON

##### Useful references
Component template should contain exactly one root element. If you are using v-if on multiple elements, use v-else-if to chain them instead.
[https://zendev.com/2018/05/07/multi-root-vue-components.html]https://zendev.com/2018/05/07/multi-root-vue-components.html
 
[http://www.developerdrive.com/2017/07/creating-a-data-table-in-vue-js/]http://www.developerdrive.com/2017/07/creating-a-data-table-in-vue-js





