package com.hw;

import com.hw.dto.User;

public interface UserService {
    void login(User user);
    String getEncodedAuthorizationHeader(User user);
}
