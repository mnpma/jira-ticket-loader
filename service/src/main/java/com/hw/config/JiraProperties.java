package com.hw.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("jira")
public class JiraProperties {
    private String projectId;
    private String url;
    private String projectKey;
    private String taskIssueTypeId;
    private String assigneeName;
    private String assigneeEmailAddress;
    private String toDoStatusId;
    private String actComponentId;
    private String reporterName;
    private String reporterEmailAddress;
    private String shouldHavePriorityId;
    private String customfield_10703Id;

    public String getCustomfield_10703Id() {
        return customfield_10703Id;
    }

    public void setCustomfield_10703Id(String customfield_10703Id) {
        this.customfield_10703Id = customfield_10703Id;
    }

    public String getShouldHavePriorityId() {
        return shouldHavePriorityId;
    }

    public void setShouldHavePriorityId(String shouldHavePriorityId) {
        this.shouldHavePriorityId = shouldHavePriorityId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getProjectKey() {
        return projectKey;
    }

    public void setProjectKey(String projectKey) {
        this.projectKey = projectKey;
    }

    public String getTaskIssueTypeId() {
        return taskIssueTypeId;
    }

    public void setTaskIssueTypeId(String taskIssueTypeId) {
        this.taskIssueTypeId = taskIssueTypeId;
    }

    public String getAssigneeName() {
        return assigneeName;
    }

    public void setAssigneeName(String assigneeName) {
        this.assigneeName = assigneeName;
    }

    public String getAssigneeEmailAddress() {
        return assigneeEmailAddress;
    }

    public void setAssigneeEmailAddress(String assigneeEmailAddress) {
        this.assigneeEmailAddress = assigneeEmailAddress;
    }

    public String getToDoStatusId() {
        return toDoStatusId;
    }

    public void setToDoStatusId(String toDoStatusId) {
        this.toDoStatusId = toDoStatusId;
    }

    public String getActComponentId() {
        return actComponentId;
    }

    public void setActComponentId(String actComponentId) {
        this.actComponentId = actComponentId;
    }

    public String getReporterName() {
        return reporterName;
    }

    public void setReporterName(String reporterName) {
        this.reporterName = reporterName;
    }

    public String getReporterEmailAddress() {
        return reporterEmailAddress;
    }

    public void setReporterEmailAddress(String reporterEmailAddress) {
        this.reporterEmailAddress = reporterEmailAddress;
    }
}
