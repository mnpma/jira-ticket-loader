package com.hw;

import com.hw.dto.SubTaskType;
import com.hw.dto.TimeTracking;

public interface TimeTrackingService {
    TimeTracking getTimeTracking(SubTaskType subTaskType, String devEstimate);
}
