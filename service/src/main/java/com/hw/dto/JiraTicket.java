package com.hw.dto;

import com.hw.dto.fields.BasePropertyId;
import com.hw.dto.fields.BasePropertyName;
import com.hw.dto.fields.Fields;
import com.hw.dto.fields.Timetracking;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JiraTicket {
    private Fields fields;

    public Fields getFields() {
        return fields;
    }

    public void setFields(Fields fields) {
        this.fields = fields;
    }

    public static JiraTicket.Builder newBuilder() {
        return new JiraTicket().new Builder();
    }

    public class Builder {
        private Builder() {
            JiraTicket.this.fields = new Fields();
        }
        public JiraTicket build() {
            return JiraTicket.this;
        }

        public JiraTicket.Builder setProjectId(String id) {
            JiraTicket.this.fields.setProject(new BasePropertyId(id));
            return this;
        }
        public JiraTicket.Builder setSummary(String summary) {
            JiraTicket.this.fields.setSummary(summary);
            return this;
        }

        public JiraTicket.Builder setIssueType(String id) {
            JiraTicket.this.fields.setIssuetype(new BasePropertyId(id));
            return this;
        }

        public JiraTicket.Builder setAssignee(String name, String email) {
            JiraTicket.this.fields.setAssignee(new BasePropertyName(name, email));
            return this;
        }
        public JiraTicket.Builder setReporter(String name, String email) {
            JiraTicket.this.fields.setReporter(new BasePropertyName(name, email));
            return this;
        }

        public JiraTicket.Builder setPriority(String id) {
            JiraTicket.this.fields.setPriority(new BasePropertyId(id));
            return this;
        }
        public JiraTicket.Builder setTimeTracking(String originalEstimate) {
            JiraTicket.this.fields.setTimetracking(new Timetracking(originalEstimate));
            return this;
        }

        public JiraTicket.Builder setDescription(String description) {
            JiraTicket.this.fields.setDescription(description);
            return this;
        }

        public JiraTicket.Builder setComponents(String... components) {
            List<BasePropertyId> componentList = Stream.of(components)
                    .map(c -> new BasePropertyId(c))
                    .collect(Collectors.toList());
            JiraTicket.this.fields.setComponents(componentList);
            return this;
        }

        public JiraTicket.Builder setCustomfield_10703(String id) {
            JiraTicket.this.fields.setCustomfield_10703(new BasePropertyId(id));
            return this;
        }

    }

}
