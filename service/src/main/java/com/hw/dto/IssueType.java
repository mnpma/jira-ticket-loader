package com.hw.dto;

public enum IssueType {
    ShouldHave("3");

    private final String id;

    IssueType(String id) {
        this.id = id;
    }
}
