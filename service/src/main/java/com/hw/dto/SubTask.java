package com.hw.dto;

public class SubTask {
    private String summary;
    private String status;
    //shouldHave - 3
    private String priority;
    //id=5 name=Sub-task
    private String issuetype;

    private TimeTracking timeTracking;

    public SubTask() {
    }

    public SubTask(String summary, String status, String priority, String issuetype, TimeTracking timeTracking) {
        this.summary = summary;
        this.status = status;
        this.priority = priority;
        this.issuetype = issuetype;
        this.timeTracking = timeTracking;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getIssuetype() {
        return issuetype;
    }

    public void setIssuetype(String issuetype) {
        this.issuetype = issuetype;
    }

    public TimeTracking getTimeTracking() {
        return timeTracking;
    }

    public void setTimeTracking(TimeTracking timeTracking) {
        this.timeTracking = timeTracking;
    }
}
