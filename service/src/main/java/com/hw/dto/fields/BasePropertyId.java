package com.hw.dto.fields;

public class BasePropertyId {
    private String id;

    public BasePropertyId() {
    }

    public BasePropertyId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
