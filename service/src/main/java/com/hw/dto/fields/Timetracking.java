package com.hw.dto.fields;

public class Timetracking {
    private String originalEstimate;

    public Timetracking() {
    }

    public Timetracking(String originalEstimate) {
        this.originalEstimate = originalEstimate;
    }

    public String getOriginalEstimate() {
        return originalEstimate;
    }

    public void setOriginalEstimate(String originalEstimate) {
        this.originalEstimate = originalEstimate;
    }
}
