package com.hw.dto.fields;

import java.util.List;

public class Fields {
    private BasePropertyId project;//10106
    private String summary;
    private BasePropertyId issuetype; //3
    private BasePropertyName assignee;
    private BasePropertyName reporter;
    private BasePropertyId priority;
    private Timetracking timetracking;
    private String description;
    private List<BasePropertyId> components;
    private BasePropertyId customfield_10703;

    public BasePropertyId getCustomfield_10703() {
        return customfield_10703;
    }

    public void setCustomfield_10703(BasePropertyId customfield_10703) {
        this.customfield_10703 = customfield_10703;
    }

    public BasePropertyId getProject() {
        return project;
    }

    public void setProject(BasePropertyId project) {
        this.project = project;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public BasePropertyId getIssuetype() {
        return issuetype;
    }

    public void setIssuetype(BasePropertyId issuetype) {
        this.issuetype = issuetype;
    }

    public BasePropertyName getAssignee() {
        return assignee;
    }

    public void setAssignee(BasePropertyName assignee) {
        this.assignee = assignee;
    }

    public BasePropertyName getReporter() {
        return reporter;
    }

    public void setReporter(BasePropertyName reporter) {
        this.reporter = reporter;
    }

    public BasePropertyId getPriority() {
        return priority;
    }

    public void setPriority(BasePropertyId priority) {
        this.priority = priority;
    }

    public Timetracking getTimetracking() {
        return timetracking;
    }

    public void setTimetracking(Timetracking timetracking) {
        this.timetracking = timetracking;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<BasePropertyId> getComponents() {
        return components;
    }

    public void setComponents(List<BasePropertyId> components) {
        this.components = components;
    }
}
