package com.hw.dto;

public class TicketError {
    private String id;
    private String line;
    private String message;

    public TicketError() {
    }

    public TicketError(String id, String line, String message) {
        this.id = id;
        this.line = line;
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
