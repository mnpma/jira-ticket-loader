package com.hw.dto;

public class TicketDto {
    private String components;
    private String summary;
    private String action;

    public TicketDto() {
    }

    public TicketDto(String components, String summary) {
        this.components = components;
        this.summary = summary;
        this.action = "Remove";
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getComponents() {
        return components;
    }

    public void setComponents(String components) {
        this.components = components;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
