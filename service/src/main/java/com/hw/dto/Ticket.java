package com.hw.dto;


import java.util.List;

public class Ticket {
    private Long id;
    private String self;
    private String key;
    //Task - 3
    private String issueType;

    //ACT - 10106
    private String project;
    //To Do - 10000
    private String status;

    //Should Have - 4
    private String priority;
    //customfield_10703 PL US - 10702
    private String region;
    //ACT - 10213
    private String components;
    private String summary;
    //241771
    //private String creator;
    //private String creatorEmailAddress = "marina.pimenova@thomsonreuters.com";
    private List<SubTask> subtasks;
    private TimeTracking timetracking;
    private Reporter reporter;
    //evgenii.podolskii@thomsonreuters.com 232642
    private String assignee;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIssueType() {
        return issueType;
    }

    public void setIssueType(String issueType) {
        this.issueType = issueType;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComponents() {
        return components;
    }

    public void setComponents(String components) {
        this.components = components;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public List<SubTask> getSubtasks() {
        return subtasks;
    }

    public void setSubtasks(List<SubTask> subtasks) {
        this.subtasks = subtasks;
    }

    public TimeTracking getTimetracking() {
        return timetracking;
    }

    public void setTimetracking(TimeTracking timetracking) {
        this.timetracking = timetracking;
    }

    public Reporter getReporter() {
        return reporter;
    }

    public void setReporter(Reporter reporter) {
        this.reporter = reporter;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", self='" + self + '\'' +
                ", key='" + key + '\'' +
                '}';
    }

    public static Builder newBuilder() {
        return new Ticket().new Builder();
    }

    public class Builder {
        private Builder(){}
        public Ticket build() {return Ticket.this;}

        public Ticket.Builder setId(Long id) {
            Ticket.this.id = id;
            return this;
        }

        public Ticket.Builder setSummary(String summary) {
            Ticket.this.summary = summary;
            return this;
        }
        public Ticket.Builder setPriority(String priority) {
            Ticket.this.priority = priority;
            return this;
        }
        public Ticket.Builder setComponents(String components) {
            Ticket.this.components = components;
            return this;
        }

        public Ticket.Builder setSubTasks(List<SubTask> subtasks) {
            Ticket.this.subtasks = subtasks;
            return this;
        }

        public Ticket.Builder setTimeTracking(TimeTracking timeTracking) {
            Ticket.this.timetracking = timetracking;
            return this;
        }
        //TODO
        //Assignee
        //Reporter
    }


}
