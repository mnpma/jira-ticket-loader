package com.hw.dto;

public enum SubTaskType {
    Dev(1.0),
    QED(0.5),
    CodeReview(0.1),
    UnitTests(0.4);

    private final double multiplicator;

    SubTaskType(double multiplicator) {
        this.multiplicator = multiplicator;
    }

    public double getMultiplicator() {
        return multiplicator;
    }
}
