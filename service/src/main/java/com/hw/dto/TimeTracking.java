package com.hw.dto;

public class TimeTracking {
    private String originalEstimate;
    private String remainingEstimate;
    private Long originalEstimateSeconds;
    private Long remainingEstimateSeconds;

    public TimeTracking() {
    }

    public TimeTracking(String originalEstimate) {
        this.originalEstimate = originalEstimate;
        this.remainingEstimate = originalEstimate;
        this.originalEstimateSeconds = Long.parseLong(this.originalEstimate) * 24 * 3600;
        this.remainingEstimateSeconds = this.originalEstimateSeconds;
    }

    public TimeTracking(String originalEstimate, String remainingEstimate) {
        this.originalEstimate = originalEstimate;
        this.remainingEstimate = remainingEstimate;
        this.originalEstimateSeconds = Long.parseLong(this.originalEstimate) * 24 * 3600;
        this.remainingEstimateSeconds = this.originalEstimateSeconds;
    }

    public Long getOriginalEstimateSeconds() {
        return originalEstimateSeconds;
    }

    public Long getRemainingEstimateSeconds() {
        return remainingEstimateSeconds;
    }

    public String getOriginalEstimate() {
        return originalEstimate;
    }

    public void setOriginalEstimate(String originalEstimate) {
        this.originalEstimate = originalEstimate;
    }

    public String getRemainingEstimate() {
        return remainingEstimate;
    }

    public void setRemainingEstimate(String remainingEstimate) {
        this.remainingEstimate = remainingEstimate;
    }
}
