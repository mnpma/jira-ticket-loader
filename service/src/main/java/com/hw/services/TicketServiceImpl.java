package com.hw.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hw.TicketService;
import com.hw.TimeTrackingService;
import com.hw.UserService;
import com.hw.config.GlobalProperties;
import com.hw.config.JiraProperties;
import com.hw.dto.*;

import com.opencsv.CSVReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;

import org.springframework.http.*;
import org.springframework.stereotype.Service;


import java.io.File;
import java.io.FileReader;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

@Service(value = "ticketService")
public class TicketServiceImpl implements TicketService {
    private static final Logger log = LoggerFactory.getLogger(TicketServiceImpl.class);
    @Autowired
    private TimeTrackingService timeTrackingService;
    @Autowired
    private UserService userService;
    @Autowired
    private User user;
    @Autowired
    private GlobalProperties global;
    @Autowired
    private JiraProperties jira;

    public static final String CSV_FILE_NAME = "csv/jira-tickets.csv";

    @Override
    public List<Ticket> load() {
        List<Ticket> tickets = new ArrayList<>();
        CSVReader reader = null;
        try {
            File csvFile = getFileFromResources(CSV_FILE_NAME);
            reader = new CSVReader(new FileReader(csvFile), ';');
            String[] line;
            List<TicketError> errors = new ArrayList<>();
            long lineId = 0L;
            reader.readNext(); //skip the header
            while ((line = reader.readNext()) != null) {
                //System.out.println("Country [id= " + line[0] + ", code= " + line[1] + " , name=" + line[2] + "]");
                Ticket t = line2Ticket(lineId++, line, errors);
                if (t == null) {
                    continue;
                }
                tickets.add(t);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return tickets;
    }

    public List<TicketDto> convertTicketToTicketDto(List<Ticket> tickets) {
        return tickets.stream().map(s-> new TicketDto(s.getComponents(), s.getSummary())).collect(Collectors.toList());
    }

    private File getFileFromResources(String fileName) {
        //Get file from resources folder
        ClassLoader classLoader = getClass().getClassLoader();
        return new File(classLoader.getResource(fileName).getFile());
    }

    private Ticket line2Ticket(long lineId, String[] line, List<TicketError> errors) {
        errors.clear();
        if (line.length != 8) {
            errors.add(new TicketError(String.valueOf(lineId), line.toString(), "Incorrect line's length"));
            return null;
        }
        List<SubTask> subTasks = new ArrayList<>(SubTaskType.values().length);
        for (SubTaskType sb : SubTaskType.values()) {
            SubTask subTask = line2SubTask(sb, line, errors);
            if (subTask == null) {
                return null;
            }
            subTasks.add(subTask);
        }
        Ticket ticket = Ticket.newBuilder().setId(lineId)
                .setComponents(line[7])
                .setPriority(line[4])
                .setSummary(line[0])
                .setSubTasks(subTasks)
                .build();
        return ticket;
    }

    private SubTask line2SubTask(SubTaskType subTaskType, String[] line, List<TicketError> errors) {
        SubTask subTask = new SubTask();
        subTask.setIssuetype("5");
        subTask.setPriority("3");
        subTask.setSummary(String.format("%s-%s", line[0], subTaskType.name()));
        subTask.setTimeTracking(timeTrackingService.getTimeTracking(subTaskType, line[1]));
        return subTask;
    }

    @Override
    public List<Ticket> getAll() {
        return null;
    }

    @Override
    public Ticket getById(Long id) {
        return null;
    }

    @Override
    public List<JiraTicket> createAll() {
        return null;
    }

    @Override
    public JiraResponse create(Ticket ticket) throws JsonProcessingException {
        JiraTicket jiraTicket = convertTicket2Json(ticket);
        ObjectMapper mapper = new ObjectMapper();
        String jiraTicketJson =  mapper.writeValueAsString(jiraTicket);

        Client client = Client.create();
        String URI = jira.getUrl() + "/issue";
        WebResource webResource = client.resource(URI);
        String encodingUsernamePassword = userService.getEncodedAuthorizationHeader(user);
        ClientResponse response = webResource.type("application/json")
                .header("Authorization", "Basic " + encodingUsernamePassword)
                .post(ClientResponse.class, jiraTicketJson);

        if (response.getStatus() != HttpStatus.CREATED.value()) {
            log.debug("Failed : HTTP error code : " + response.getStatus());
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
        }
        String output = response.getEntity(String.class);

        JsonParser parser = JsonParserFactory.getJsonParser();
        Map<String, Object> map = parser.parseMap(output);

        JiraResponse jiraResponse = new JiraResponse();
        jiraResponse.setId(map.get("id").toString());
        jiraResponse.setKey(map.get("key").toString());
        jiraResponse.setSelf(map.get("self").toString());
        log.debug(jiraResponse.toString());
        client.destroy();
        response.close();
        return jiraResponse;
    }

    @Override
    public int delete(String issueKey) {
        throw new RuntimeException("Failed : HTTP error code : "
                + 404);
/*
        Client client = Client.create();
        //DELETE /rest/api/2/issue/{issueIdOrKey}
        String URI = jira.getUrl() + "/issue" + issueKey;
        WebResource webResource = client.resource(URI);

        String encodingUsernamePassword = userService.getEncodedAuthorizationHeader(user);
        //If the issue has subtasks you must set the parameter deleteSubtasks=true to delete the issue.
        MultivaluedMap<String, String> params = new MultivaluedMapImpl();
        params.add("deleteSubtasks", "true");
        ClientResponse response = webResource
                .queryParams(params)
                .header("Authorization", "Basic " + encodingUsernamePassword)
                .delete(ClientResponse.class);

        //204 Returned if the issue was removed successfully.
        if (response.getStatus() != 204) {
            log.debug("Failed : HTTP error code : " + response.getStatus());
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
        }
        return response.getStatus();
*/
    }

    protected JiraTicket convertTicket2Json(Ticket ticket) {
        JiraTicket jt = new JiraTicket().newBuilder()
                .setProjectId(jira.getProjectId())
                .setSummary(ticket.getSummary()) //
                .setIssueType(jira.getTaskIssueTypeId())
                .setAssignee(jira.getAssigneeName(), jira.getAssigneeEmailAddress())
                .setReporter(jira.getReporterName(), jira.getReporterEmailAddress())
                .setPriority(jira.getShouldHavePriorityId())
                .setTimeTracking(ticket.getTimetracking().getOriginalEstimate()) //
                .setDescription("Description should go here")
                .setComponents(jira.getActComponentId())
                .setCustomfield_10703(jira.getCustomfield_10703Id())
                .build();

        return jt;
    }

    @Override
    public Project healthCheck() {
        //GET /rest/api/2/project/{projectIdOrKey}
        //http://jira.ald.int.westgroup.com/rest/api/latest/project/ACT
        Client client = Client.create();
        String URI = jira.getUrl() + "project/" + jira.getProjectKey();
        WebResource webResource = client.resource(URI);

        String encodingUsernamePassword = userService.getEncodedAuthorizationHeader(user);

        ClientResponse response = webResource.type("application/json")
                .header("Authorization", "Basic " + encodingUsernamePassword)
                .get(ClientResponse.class);

        if (response.getStatus() != 200) {
            log.debug("Failed : HTTP error code : " + response.getStatus());
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
        }
        String output = response.getEntity(String.class);

        JsonParser parser = JsonParserFactory.getJsonParser();
        Map<String, Object> map = parser.parseMap(output);
        Project project = json2Project(map);
        log.debug(project.toString());
        client.destroy();
        response.close();
        return project;
    }

    protected Project json2Project(Map<String, Object> json) {
        Project project = new Project();
        project.setId(json.get("id").toString());
        project.setSelf(json.get("self").toString());
        project.setKey(json.get("key").toString());
        project.setName(json.get("name").toString());
        project.setProjectTypeKey(json.get("projectTypeKey").toString());
        return project;
    }

}
