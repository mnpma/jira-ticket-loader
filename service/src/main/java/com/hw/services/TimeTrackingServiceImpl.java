package com.hw.services;

import com.hw.TimeTrackingService;
import com.hw.dto.SubTaskType;
import com.hw.dto.TimeTracking;
import org.springframework.stereotype.Service;

@Service(value="timeTrackingService")
public class TimeTrackingServiceImpl implements TimeTrackingService {
    //devEstimate in Days
    @Override
    public TimeTracking getTimeTracking(SubTaskType subTaskType, String devEstimate) {
        double days  = Integer.parseInt(devEstimate) * subTaskType.getMultiplicator();
        String originalEstimate = String.valueOf(Math.round(days));
        TimeTracking timeTracking = new TimeTracking(originalEstimate, originalEstimate);
        return timeTracking;
    }
}
