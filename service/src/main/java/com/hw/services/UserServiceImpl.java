package com.hw.services;

import com.hw.UserService;
import com.hw.dto.User;
import org.springframework.stereotype.Service;
import sun.misc.BASE64Encoder;


@Service(value = "userService")
public class UserServiceImpl implements UserService {
    @Override
    public void login(User user) {

    }

    @Override
    public String getEncodedAuthorizationHeader(User user) {
        BASE64Encoder enc = new sun.misc.BASE64Encoder();
        String userPassword = user.getUsername() + ":" + user.getPassword();
        String encoding = enc.encode(userPassword.getBytes());
        return encoding;
    }
}
