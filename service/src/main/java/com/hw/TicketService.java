package com.hw;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.hw.dto.*;

import java.util.List;

public interface TicketService {
    List<Ticket> load();
    List<TicketDto> convertTicketToTicketDto(List<Ticket> tickets);

    List<Ticket> getAll();

    Ticket getById(Long id);

    List<JiraTicket> createAll();

    JiraResponse create(Ticket ticket) throws JsonProcessingException;
    int delete(String issueKey);

    Project healthCheck();
}
